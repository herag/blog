<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="main.css" type="text/css" />
<link rel="stylesheet" href="blog.css" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="Subscribe to this page..." href="feed.rss" />
<title>Inherent Selfishness of the Individual: an essay</title>
</head><body>
<div id="divbodyholder">
<div class="headerholder"><div class="header">
<div id="title">
<h1 class="nomargin"><a class="ablack" href="https://poetgrant.press/index.html">PoetGrant's Press</a></h1>
<div id="description">Decentralized | Voluntary | Agora</div>
</div></div></div>
<div id="divbody"><div class="content">
<!-- entry begin -->
<h3><a class="ablack" href="inherent-selfishness-of-the-individual-an-essay.html">
Inherent Selfishness of the Individual: an essay
</a></h3>
<!-- bashblog_timestamp: #201905020946.59# -->
<div class="subtitle">May 02, 2019 &mdash; 
Grant Ford
</div>
<!-- text begin -->

        <p>Growing up I learned a lot about a term called "the individual". I learned that I was an individual and that my 
rights were inherent in my existence. "I think therefore I am" made sense to me. The word "I" and "me" made sense to me. Until 
recently I never thought of myself as a "white, cis male" or anything really other than simply an individual.</p>

<hr>
        
        <p>But what <em>is</em> the individual? That is the question I would like to attempt to answer in this essay.</p>
        
        <p>The Individual is defined by the <a href="http://www.oed.com/">OED (Oxford English Dictionary)</a> as:</p>
        <quotation cite="https://en.oxforddictionaries.com/definition/individual">1 A single human being as distinct from a 
group.<br/>
        1.1 A single member of a class.<br/>
        1.2 <em>informal with adjective</em> A person of a specified kind.<br/>
        1.3 A distinctive or original person.</q>
        
        <p>The definition we will proceed with is 1.3, "a distinctive or original person".
        
        <p>One of the most well known heralders of the Individual as a philosophical subject was Ayn Rand. Love her or hate 
her, she wrote more about how she defined the Individual than almost anyone I have ever read or heard. Her capstone work, <a 
href="https://www.aynrand.org/novels/atlas-shrugged"><em>Atlas Shrugged</em></a>, was a novel that focused heavily on the power 
behind the individual's will to succeed and build society around them. From her novel the main point I have taken is that the 
Individual is not a cog in a machine, but rather its own distinctive machine built to work and create. Rand chose to focus on 
the Individual's selfishness as a virtue rather than as an evil, which has gained much criticism from all fields of study.</p>
        
        <p>Selfishness, in my studied opinion, is a word with conflicting definitions. Whereas it is defined in the OED as <a 
href="https://en.oxforddictionaries.com/definition/selfishness">"The quality or state of being selfish; lack of consideration 
for other people."</a>, Rand claims it is more central to the individual. Selfishness, in her terms, is actually simply the 
state of being an individual. One could argue that everything we do is against our own demise, as that is a biological response 
to such a situation. However some might say that the individual occasionally sacrifices themselves to a cause or for another, 
as if that is a retort to the claim that selfishness is virtuous and natural. In truth, I must ask, why is that person 
sacrificing themselves? Perhaps their desire to see another person live or succeed is stronger than their own desire to live or 
succeed. So if they pursue their own desire, is that not also selfishness?</p>
        
        <p>I would argue that a person does not do anything out of a lack of desire; be it a desire to an alternate end or to 
the end that is expected. A person does not give up eating meat, for instance, simply so that a cow or chicken might survive 
one more day. No, that person gives up eating meat because they desire that fewer cows and chickens will be killed. These two 
things may sound synonymous, but I urge you to consider the subtle difference because it is very important. You, as an 
individual, cannot do something against your own desires unless there is something that you desire more.</p>
        
        <p>Many also claim that selfishness is a means to achieve immediate satisfaction, and thus is destructive in the end. 
While some people will do this, the ends that they achieve prove that they were not being truly selfish at all! If you want to 
feel really good, you may try to achieve that goal by injecting some sort of euphoric drug. However, after becoming addicted 
and ruining your life and the lives of those around you, you will find that your plan to fulfill that particular desire was not 
very selfish at all. Selfishness as a virtue requires that you work for an end that benefits you as an Individual completely, 
removing as many downsides to your actions as is possible.</p>
        
        <p>So selfishness is not immediate gratification within this definition. True selfishness cannot have an ends to hurt 
the Individual. It must always benefit the Individual. When you identify this as a core principle of selfishness, then you may 
look outward and try to identify how this affects those around this Selfish Individual. I have tried to think of a way that 
selfishness could negatively affect anyone.</p>
        
        <p>So let's say that a man wants to be self-employed. How could this possibly negatively affect those around him? Let's 
try. So they start a business selling phone cases. He begins making them himself with a 3D printer, but eventually he has too 
many orders to make them all on his own. So he hires another individual to help him be more productive. He pays that person 
$10/hr.</p>
        
        <p>So let's say that $10/hr is barely enough money to live working 40 hours a week. Is he hurting that person? No. He 
gave them his tools to be productive and agreed to pay what he thinks that person is worth to him. This is a voluntary 
transaction. Since it is voluntary, it is impossible for this agreement to negatively affect either party. However, this is 
where things get a little more complex.</p>
        
        <p>The above is a figurative example, but we will now discuss a real example. Currently, a single individual cannot 
work more than 40 hours in a week without the government requiring a mandated increase in wage. Already, this transaction has 
become involuntary, therefore against the self-interest of the employer. So, the employer has a choice to make; either pay 1.5 
times the amount that was voluntarily agreed to or hire another part time worker. Both situations are undesirable. When I was 
15, I began working for a Chinese restaraunt. I was working 40 hours per week, but I was saving for a truck and wanted to work 
more. I was going to the local community colled to finish my highschool, so I was able to make a schedule that works with my 
desire to work more than 40 hours per week. My boss said no because she was unwilling to pay me more. Instead of complaining 
about how cheap she was and accepting that transaction, I devised a plan. It was mutually beneficial.</p>
        
        <p>I agreed to be hired again for her husband's company, then be contracted to the restaraunt to wash dishes for about 
20 hours a week. This enabled her to use my productivity for roughly 60 hours per week, and I was able to buy the truck (used 
of course) within a month. I found, however, that I enjoyed working so much and making as much as I was making, so she and I 
agreed to keep this arrangement until I went to college.</p>
        
        <p>This experience made me realize a fallacy in my own logic. There was a time that I would praise the Unions in the US 
for giving the American worker the 40 hour work week. Now I cannot offer them praise. In fact, I resent it. They have limited 
the Individual's capacity for full productivity. Not to mention the coercive nature of the minimum wage, but that would add too 
much complexity to this essay. In the end, what can be learned is that my own selfishness pushes me to work more hours to 
achieve a goal. I become a more motivated employee if I have a goal to achieve. In the same way, I owe the employer no more 
than what he pays for, which is part of the terms we both agree to in a voluntary exchange.</p>
        
        <p>This approach to the ideal of selfishness then creates an atmosphere in which the Individual can truly pursue 
happiness. This whole essay then shapes the definition of the "Individual" as a concept. When you identitfy yourself, you are 
who you are. You are not part of a group inherently. You may exist within the framework of a group, but as an Individual you 
also stand apart from the group and exist whollely independently from that group. This is a concept that I have tried to flesh 
out before because it is an empowering concept. Being who I am, I know what I am capable of. Being part of a group will only 
work to weaken the Individual, in my perspective because that person can then throw off the bonds of responsibility and accept 
the "duty" to the group goal. A group of white, cis males may rely on each other for one reason or another and have a certain 
amount of combined productive power in one way or another, but each individuals unchained from the group can operate 
indepentently to reach their full productivity and achieve their own goals. Those goals may or may not benefit those around 
them, but it shouldn't matter as long as it is not hurting those around them.</p>
        
        <p>So that is why I have come to hold the Individual and the virtue of selfishness in high regard. They are 
foundational to human experience. Even within a group, we each experience life individually and can't help but to work for our 
own ends. I work a job to support my own desires, which in turn supports the desires of those around me. My selfishness is 
virtuous because it is the only thing that I can truly control.</p>
        
        <p>Feel free to contact me on <a href="https://fosstodon.org/@poetgrant" title="fosstodon">Fosstodon</a> if you have 
any thoughts or questions.</p>
















<!-- text end -->
<!-- entry end -->
</div>
<div id="footer">CC by-nc-nd <a href="http://dobbs.town/@herag">Grant Ford</a> &mdash; <a href="mailto:herag&#64;riseup&#46;net">herag&#64;riseup&#46;net</a><br/>
Generated with <a href="https://github.com/cfenollosa/bashblog">bashblog</a>, a single bash script to easily create blogs like this one</div>
</div></div>
</body></html>
